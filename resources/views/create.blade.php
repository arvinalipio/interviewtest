@extends('layout')

@section('content')

<div class="card uper content-center">
  <div class="card-header">
    Add Games Data
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" id="add-game" action="{{ route('games.store') }}">
          <div class="block p-6 rounded-lg shadow-lg bg-white max-w-sm form-group content-center">
              @csrf
              <label for="country_name" class="form-label inline-block mb-2 text-gray-700">Game Name:</label>
              <input type="text" class="form-control
              block
            w-full
            px-3
            py-1.5
            text-base
            font-normal
            text-gray-700
            bg-white bg-clip-padding
            border border-solid border-gray-300
            rounded
            transition
            ease-in-out
            m-0
            focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
              " name="name"/>
          </div>
          <div class="block p-6 rounded-lg shadow-lg bg-white max-w-sm form-group">
              <label for="cases" class="form-label inline-block mb-2 text-gray-700">Price :</label>
              <input type="text" class="form-control
              block
            w-full
            px-3
            py-1.5
            text-base
            font-normal
            text-gray-700
            bg-white bg-clip-padding
            border border-solid border-gray-300
            rounded
            transition
            ease-in-out
            m-0
            focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" name="price"/>
          </div>
          <button type="submit" id="add-submit" class="
      px-6
      py-2.5
      bg-purple-600
      text-white
      font-medium
      text-xs
      leading-tight
      uppercase
      rounded
      shadow-md
      hover:bg-purple-700 hover:shadow-lg
      focus:bg-purple-700 focus:shadow-lg focus:outline-none focus:ring-0
      active:bg-purple-800 active:shadow-lg
      transition
      duration-150
      ease-in-out">Add Game</button>
      </form>
  </div>
</div>
@endsection